<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', 'IndexController@index');

Route::get('/admin', 'AdminController@index');
Route::get('/admin/posts', 'AdminController@posts');
Route::post('/admin/create-post', 'AdminController@createPost');
Route::post('/admin/update-post', 'AdminController@updatePost');
Route::post('/admin/delete-post', 'AdminController@deletePost');
Route::get('/admin/patents', 'AdminController@patents');
Route::post('/admin/create-patent', 'AdminController@createPatent');
Route::post('/admin/update-patent', 'AdminController@updatePatent');
Route::post('/admin/delete-patent', 'AdminController@deletePatent');
Route::get('/admin/varieties', 'AdminController@varieties');
Route::post('/admin/create-variety', 'AdminController@createVariety');
Route::post('/admin/update-variety', 'AdminController@updateVariety');
Route::post('/admin/delete-variety', 'AdminController@deleteVariety');

Route::get('/news', 'IndexController@posts');
Route::get('/news/category/{category_alias}', 'IndexController@posts');
Route::get('/patents', 'IndexController@patents');
Route::get('/varieties', 'IndexController@varieties');
Route::get('/{item_alias}', 'IndexController@item');

