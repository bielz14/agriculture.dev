<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
        Category::create([
            'title' => 'Агрокультуры',
            'translit' => 'agricultural'
        ]);

        Category::create([
            'title' => 'Злаковые культуры',
            'translit' => 'cereals'
        ]);
    }
}
