<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
        User::create([
            'firstname' => 'Брат',
            'lastname' => 'Гордиенко',
            'email' => 'studentik@deneg.net',
            'password' => bcrypt('1111'),
            'role' => 'admin'
        ]);
    }
}
