$(document).ready(function(){
	$('#tab-list').on('click', function(event){
		event.preventDefault();
		if (!$('#tab-list.active').length) {
			$('#tab-list').addClass('active');
			$('.row-fluid.list').show();

			if ($('#tab-create').length) {
				$('#tab-create').removeClass('active');
				$('.row-fluid.create-update').hide();
			} else if ($('#tab-update').length) {
				$('#tab-update').removeClass('active');
				$('.row-fluid.create-update').hide();
			}
			
		}
	});

	$('#tab-create').on('click', function(event){
		event.preventDefault();
		if (!$('#tab-create.active').length) {
			$('#tab-create').addClass('active');
			$('#tab-list').removeClass('active');

			$('.row-fluid.create-update').show();
			$('.row-fluid.list').hide();
		}
	});

	$('#tab-update').on('click', function(event){
		event.preventDefault();
		if (!$('#tab-update.active').length) {
			$('#tab-update').addClass('active');
			$('#tab-list').removeClass('active');

			$('.row-fluid.create-update').show();
			$('.row-fluid.list').hide();
		}
	});
});

