<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:08 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxSiteContent
 * 
 * @property int $id
 * @property string $title
 *
 * @package App\Models
 */
class Category extends Eloquent
{
	protected $table = 'categories';
	protected $primaryKey = 'id';
	public $timestamps = false;

	protected $casts = [

	];

	protected $fillable = [
		 'title',
	];

	public function post()
    {
    	return $this->hasMany('App\Models\Post', 'category_id');
    }
}
