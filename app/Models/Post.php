<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:08 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxSiteContent
 * 
 * @property int $id
 * @property int $category_id
 * @property string $short_description
 * @property string $description
 * @property string $image
 * @property string $image_title
 * @property string $meta_title
 * @property string $meta_description
 * @property string $translit
 *
 * @package App\Models
 */
class Post extends Eloquent
{
	protected $table = 'posts';
	protected $primaryKey = 'id';
	public $timestamps = true;

	protected $casts = [

	];

	protected $fillable = [
		 'title',
		 'category_id',
		 'short_description',
		 'description',
		 'image',
		 'image_title',
		 'meta_title',
		 'meta_description',
		 'translit'
	];

	public function category()
    {
    	return $this->belongsTo('App\Models\Category', 'category_id');
    }
}
