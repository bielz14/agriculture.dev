<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Oct 2018 14:13:08 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class KpxzxSiteContent
 * 
 * @property int $id
 * @property int $registration_number
 * @property int $variety_id
 * @property string $patent_registration_date
 * @property string $owner
 * @property string $image
 *
 * @package App\Models
 */
class Patent extends Eloquent
{
	protected $table = 'patents';
	protected $primaryKey = 'id';
	public $timestamps = true;

	protected $casts = [

	];

	protected $fillable = [
		 'registration_number',
		 'variety_id',
		 'patent_registration_date',
		 'owner',
		 'image'
	];

	public function variety()
    {
    	return $this->belongsTo('App\Models\Variety', 'variety_id');
    }
}
