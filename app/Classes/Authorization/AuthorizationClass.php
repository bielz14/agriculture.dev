<?php

namespace App\Classes\Authorization;

use Dlnsk\HierarchicalRBAC\Authorization;


/**
 *  This is example of hierarchical RBAC authorization configiration.
 */

class AuthorizationClass extends Authorization
{

	public function getPermissions() {
		return [
			'viewAdminPanel' => [
				'description' => 'View admin-panel',
			    ],
			'createPost' => [
				'description' => 'Created post',
			    ],
			'updatePost' => [
					'description' => 'Edit any post',
				],
			'deletePost' => [
					'description' => 'Delete any post',
				],
			'createPatent' => [
				'description' => 'Created patent',
			    ],
			'updatePatent' => [
					'description' => 'Edit any patent',
				],
			'deletePatent' => [
					'description' => 'Delete any patent',
				],
			'createVariety' => [
				'description' => 'Created variety',
			    ],
			'updateVariety' => [
					'description' => 'Edit any variety',
				],
			'deleteVariety' => [
					'description' => 'Delete any variety',
				],
		];
	}

	public function getRoles() {
		return [
			'user' => [
					
				],
		];
	}


	/**
	 * Methods which checking permissions.
	 * Methods should be present only if additional checking needs.
	 */

	/*public function editOwnPost($user, $post) {
		$post = $this->getModel(\App\Post::class, $post);  // helper method for geting model

		return $user->id === $post->user_id;
	}*/

}
