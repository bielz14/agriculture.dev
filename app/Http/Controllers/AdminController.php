<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use App\Models\Post;
use App\Models\Category;
use App\Models\Patent;
use App\Models\Variety;

class AdminController extends Controller
{
    protected function validatorCreatePost(array $data)
    { 
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'image' => 'required|mimes:jpeg,bmp,png',
            'title' => 'required|string|max:255',
            'category_id' => 'required|integer|',
            'short_description' => 'required|string|max:255',
            'description' => 'required|string',
            'image_title' => 'required|string|max:255',
            'meta_title' => 'required|string|max:255',
            'meta_description' => 'required|string|max:255',
            'translit' => 'required|string|unique:posts|max:255',
        ]);

        $niceNames = [
            'image' => '"Изображение"',
            'title' => '"Заголовок"',
            'category_id' => '"Категория"',
            'short_description' => '"Короткое описание"',
            'description' => '"Описание"',
            'image_title' => '"Заголовок изображения"',
            'meta_title' => '"Мета-заголовок"',
            'meta_description' => '"Мета-описание"',
            'translit' => '"Транслит"',
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    protected function validatorUpdatePost(array $data)
    { 
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'image' => 'mimes:jpeg,bmp,png',
            'title' => 'required|string|max:255',
            'category_id' => 'required|integer|',
            'short_description' => 'required|string|max:255',
            'description' => 'required|string',
            'image_title' => 'required|string|max:255',
            'meta_title' => 'required|string|max:255',
            'meta_description' => 'required|string|max:255',
            'translit' => 'required|string|unique:posts|max:255',
        ]);

        $niceNames = [
            'image' => '"Изображение"',
            'title' => '"Заголовок"',
            'category_id' => '"Категория"',
            'short_description' => '"Короткое описание"',
            'description' => '"Описание"',
            'image_title' => '"Заголовок изображения"',
            'meta_title' => '"Мета-заголовок"',
            'meta_description' => '"Мета-описание"',
            'translit' => '"Транслит"',
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    protected function validatorCreatePatent(array $data)
    { 
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'image' => 'required|mimes:jpeg,bmp,png',
            'registration_number' => 'required|integer|',
            'patent_registration_date' => 'required|string|max:255',
            'owner' => 'required|string|max:255',
            'variety_id' => 'required|integer|',
        ]);

        $niceNames = [
            'image' => '"Изображение"',
            'registration_number' => '"Регистрационный номер"',
            'patent_registration_date' => '"Дата регистрации патента"',
            'owner' => '"Обладатель патента"',
            'variety_id' => '"Сорт"',
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    protected function validatorUpdatePatent(array $data)
    { 
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'image' => 'mimes:jpeg,bmp,png',
            'registration_number' => 'required|integer|',
            'patent_registration_date' => 'required|string|max:255',
            'owner' => 'required|string|max:255',
            'variety_id' => 'required|integer|',
        ]);

        $niceNames = [
            'image' => '"Изображение"',
            'registration_number' => '"Регистрационный номер"',
            'patent_registration_date' => '"Дата регистрации патента"',
            'owner' => '"Обладатель патента"',
            'variety_id' => '"Сорт"',
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

        protected function validatorCreateVariety(array $data)
    { 
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'image' => 'required|mimes:jpeg,bmp,png',
            'title' => 'required|string|max:255',
            'short_description' => 'required|string|max:255',
            'description' => 'required|string',
            'image_title' => 'required|string|max:255',
            'meta_title' => 'required|string|max:255',
            'meta_description' => 'required|string|max:255',
            'translit' => 'required|string|unique:varieties|max:255',
        ]);

        $niceNames = [
            'image' => '"Изображение"',
            'title' => '"Заголовок"',
            'short_description' => '"Короткое описание"',
            'description' => '"Описание"',
            'image_title' => '"Заголовок изображения"',
            'meta_title' => '"Мета-заголовок"',
            'meta_description' => '"Мета-описание"',
            'translit' => '"Транслит"',
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    protected function validatorUpdateVariety(array $data)
    { 
        $messages = [
            'required' => 'Поле :attribute является обязательным для заполнения',
        ];

        $validator = Validator::make($data, [
            'image' => 'mimes:jpeg,bmp,png',
            'title' => 'required|string|max:255',
            'category_id' => 'required|integer|',
            'short_description' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'image_title' => 'required|string|max:255',
            'meta_title' => 'required|string|max:255',
            'meta_description' => 'required|string|max:255',
            'translit' => 'required|string|unique:varieties|max:255',
        ]);

        $niceNames = [
            'image' => '"Изображение"',
            'title' => '"Заголовок"',
            'short_description' => '"Короткое описание"',
            'description' => '"Описание"',
            'image_title' => '"Заголовок изображения"',
            'meta_title' => '"Мета-заголовок"',
            'meta_description' => '"Мета-описание"',
            'translit' => '"Транслит"',
        ];

        $validator->setAttributeNames($niceNames); 
        
        return $validator;
    }

    public function index(Request $request)
    {  
        if (!is_null($request->user()) && $request->user()->can('viewAdminPanel')) {
            return view('admin.index');
        } else if (\Auth::User()) {
            return redirect('/');
        } else {
            return redirect('login');
        }
    }

    public function posts(Request $request)
    {   
        if (!is_null($request->user()) && $request->user()->can('viewAdminPanel')) {
            $posts = Post::orderBy('created_at', 'desc')->paginate(3);
            $categories = Category::all();

            $itemId = $request->id;
            if (!is_null($itemId)) {
                $item = Post::find($itemId);
                return view('admin.posts', [
                    'posts' => $posts,
                    'categories' => $categories,
                    'item' => $item
                ]);
            }

            return view('admin.posts', [
                'posts' => $posts,
                'categories' => $categories
            ]);
        } else if (\Auth::User()) {
            return redirect('/');
        } else {
            return redirect('login');
        }
    }

    public function createPost(Request $request)
    {       
        if (!is_null($request->user()) && $request->user()->can('createPost')) {
            $request->session()->flash('post', true);

            $this->validatorCreatePost($request->all())->validate();

            $post = Post::create(array(
                    'title'  => $request->title,
                    'category_id'  => $request->category_id,
                    'short_description'  => $request->short_description,
                    'description'  => $request->description,
                    'image'  => '',
                    'image_title'  => $request->image_title,
                    'meta_title'  => $request->meta_title,
                    'meta_description'  => $request->meta_description,
                    'translit'  => $request->translit
            ));

            $dirPath = public_path() . '/images/' . $request->alias;
            if (!File::exists($dirPath)) {
                File::makeDirectory($dirPath); 
            }

            if ($post) {
                $fileExtension = Input::file('image')->getClientOriginalExtension();
                $filename = $request->translit . '_' . $post->id . '.' . $fileExtension;
                $post->image = $filename;
                if ($post->save()) {
                    if (!Input::file('image')->move($dirPath, $filename)) {
                        return 'Сообщение об ошибке!';
                    }
                }
                $request->session()->forget('post');
                return back();
            } else {
                return back()->withInput();
            }
        } else if (\Auth::User()) {
            return redirect('/');
        } else {
            return redirect('login');
        }
    }

    public function updatePost(Request $request)
    {       
        if (!is_null($request->user()) && $request->user()->can('updatePost')) {
            $request->session()->flash('post', true);

            $this->validatorUpdatePost($request->all())->validate();

            $itemId = $request->itemId;
            if (isset($itemId) && !is_null($itemId)) { 
                $post = Post::find($itemId);
                if ($post->count()) {
                    $post->title = $request->title;
                    $post->category_id = $request->category_id;
                    $post->description = $request->description;
                    $post->short_description = $request->short_description;

                    if (Input::file('image')) {
                        $fileExtension = Input::file('image')->getClientOriginalExtension();
                        $filename = $request->translit . '_' . $post->id . '.' . $fileExtension;
                        $post->image = $filename;
                    }

                    $post->image_title = $request->image_title;
                    $post->meta_title = $request->meta_title;
                    $post->meta_description = $request->meta_description;
                    $post->translit = $request->translit;
                    if ($post->save()) {
                        if (Input::file('image')) {
                            $dirPath = public_path() . '/images/' . $request->alias;
                            if (!File::exists($dirPath)) {
                                File::makeDirectory($dirPath); 
                            }

                            if (!Input::file('image')->move($dirPath, $filename)) {
                                return 'Сообщение об ошибке!';
                            } 
                        }
                        return back();
                    }
                }
            }

            return back()->withInput();
        } else if (\Auth::User()) {
            return redirect('/');
        } else {
            return redirect('login');
        }
    }


    public function deletePost(Request $request)
    {   
        if (!is_null($request->user()) && $request->user()->can('deletePost')) {
            $itemId = $request->id;
            if (!is_null($itemId)) {
                $post = Post::find($itemId);
                if ($post) {
                    $post->delete();
                }
            }

            return back();
        } else if (\Auth::User()) {
            return redirect('/');
        } else {
            return redirect('login');
        }
    }

    public function patents(Request $request)
    { 
        if (!is_null($request->user()) && $request->user()->can('viewAdminPanel')) {
            $varieties = Variety::all();
            if (!$varieties->count()) {
                $notExistsVarietes = true;
                return view('admin.patents', [
                    'notExistsVarietes' => $notExistsVarietes
                ]); 
            } else {
                $patents = Patent::orderBy('created_at', 'desc')->paginate(3);
            
                $itemId = $request->id;
                if (!is_null($itemId)) {
                    $item = Patent::find($itemId);
                    return view('admin.patents', [
                        'patents' => $patents,
                        'varieties' => $varieties,
                        'item' => $item
                    ]);
                }

                return view('admin.patents', [
                    'patents' => $patents,
                    'varieties' => $varieties
                ]);  
            }
        } else if (\Auth::User()) {
            return redirect('/');
        } else {
            return redirect('login');
        }
    }

    public function createPatent(Request $request)
    {       
        if (!is_null($request->user()) && $request->user()->can('createPatent')) {
            $request->session()->flash('patent', true);

            $this->validatorCreatePatent($request->all())->validate();

            $patent = Patent::create(array(
                    'registration_number'  => $request->registration_number,
                    'variety_id'  => $request->variety_id,
                    'patent_registration_date' => $request->patent_registration_date,
                    'owner' => $request->owner,
                    'image'  => ''
            ));

            $dirPath = public_path() . '/images/' . $request->alias;
            if (!File::exists($dirPath)) {
                File::makeDirectory($dirPath); 
            }

            if ($patent) {
                $fileExtension = Input::file('image')->getClientOriginalExtension();
                $filename = $request->translit . '_' . $patent->id . '.' . $fileExtension;
                $patent->image = $filename;
                if ($patent->save()) {
                    if (!Input::file('image')->move($dirPath, $filename)) {
                        return 'Сообщение об ошибке!';
                    }
                }
                $request->session()->forget('patent');
                return back();
            } else {
                return back()->withInput();
            }
        } else if (\Auth::User()) {
            return redirect('/');
        } else {
            return redirect('login');
        }
    }

    public function updatePatent(Request $request)
    {       
        if (!is_null($request->user()) && $request->user()->can('updatePatent')) {
            $request->session()->flash('patent', true);

            $this->validatorUpdatePatent($request->all())->validate();

            $itemId = $request->itemId;
            if (isset($itemId) && !is_null($itemId)) { 
                $patent = Patent::find($itemId);
                if ($patent->count()) {
                    $patent->registration_number = $request->registration_number;
                    $patent->patent_registration_date = $request->patent_registration_date;
                    $patent->owner = $request->owner;
                    $patent->variety_id = $request->variety_id;

                    if (Input::file('image')) {
                        $fileExtension = Input::file('image')->getClientOriginalExtension();
                        $filename = $request->translit . '_' . $patent->id . '.' . $fileExtension;
                        $patent->image = $filename;
                    }

                    if ($patent->save()) {
                        if (Input::file('image')) {
                            $dirPath = public_path() . '/images/' . $request->alias;
                            if (!File::exists($dirPath)) {
                                File::makeDirectory($dirPath); 
                            }

                            if (!Input::file('image')->move($dirPath, $filename)) {
                                return 'Сообщение об ошибке!';
                            } 
                        }
                        return back();
                    }
                }
            }

            return back()->withInput();
        } else if (\Auth::User()) {
            return redirect('/');
        } else {
            return redirect('login');
        }
    }


    public function deletePatent(Request $request)
    {   
        if (!is_null($request->user()) && $request->user()->can('deletePatent')) {
            $itemId = $request->id;
            if (!is_null($itemId)) {
                $patent = Patent::find($itemId);
                if ($patent) {
                    $patent->delete();
                }
            }

            return back();
        } else if (\Auth::User()) {
            return redirect('/');
        } else {
            return redirect('login');
        }
    }

    public function varieties(Request $request)
    { 
        if (!is_null($request->user()) && $request->user()->can('viewAdminPanel')) {
            $varieties = Variety::orderBy('created_at', 'desc')->paginate(3);
            $categories = Category::all();

            $itemId = $request->id;
            if (!is_null($itemId)) {
                $item = Variety::find($itemId);
                return view('admin.varieties', [
                    'varieties' => $varieties,
                    'categories' => $categories,
                    'item' => $item
                ]);
            }

            return view('admin.varieties', [
                'varieties' => $varieties,
                'categories' => $categories
            ]);
        } else if (\Auth::User()) {
            return redirect('/');
        } else {
            return redirect('login');
        }
    }

    public function createVariety(Request $request)
    {    
        if (!is_null($request->user()) && $request->user()->can('createVariety')) {
            $request->session()->flash('variety', true);

            $this->validatorCreateVariety($request->all())->validate();

            $variety = Variety::create(array(
                    'title'  => $request->title,
                    'category_id'  => $request->category_id,
                    'short_description'  => $request->short_description,
                    'description'  => $request->description,
                    'image'  => '',
                    'image_title'  => $request->image_title,
                    'meta_title'  => $request->meta_title,
                    'meta_description'  => $request->meta_description,
                    'translit'  => $request->translit
            ));

            $dirPath = public_path() . '/images/' . $request->alias;
            if (!File::exists($dirPath)) {
                File::makeDirectory($dirPath); 
            }

            if ($variety) {
                $fileExtension = Input::file('image')->getClientOriginalExtension();
                $filename = $request->translit . '_' . $variety->id . '.' . $fileExtension;
                $variety->image = $filename;
                if ($variety->save()) {
                    if (!Input::file('image')->move($dirPath, $filename)) {
                        return 'Сообщение об ошибке!';
                    }
                }
                $request->session()->forget('variety');
                return back();
            } else {
                return back()->withInput();
            }
        } else if (\Auth::User()) {
            return redirect('/');
        } else {
            return redirect('login');
        }
    }

    public function updateVariety(Request $request)
    {       
        if (!is_null($request->user()) && $request->user()->can('updateVariety')) {
            $request->session()->flash('variety', true);

            $this->validatorUpdateVariety($request->all())->validate();

            $itemId = $request->itemId;
            if (isset($itemId) && !is_null($itemId)) { 
                $variety = Variety::find($itemId);
                if ($variety->count()) {
                    $variety->title = $request->title;
                    $variety->description = $request->description;
                    $variety->short_description = $request->short_description;

                    if (Input::file('image')) {
                        $fileExtension = Input::file('image')->getClientOriginalExtension();
                        $filename = $request->translit . '_' . $variety->id . '.' . $fileExtension;
                        $variety->image = $filename;
                    }

                    $variety->image_title = $request->image_title;
                    $variety->meta_title = $request->meta_title;
                    $variety->meta_description = $request->meta_description;
                    $variety->translit = $request->translit;
                    if ($variety->save()) {
                        if (Input::file('image')) {
                            $dirPath = public_path() . '/images/';
                            if (!File::exists($dirPath) && Input::file('image')) {
                                File::makeDirectory($dirPath); 
                            }

                            if (!Input::file('image')->move($dirPath, $filename)) {
                                return 'Сообщение об ошибке!';
                            }
                        }

                        return back();
                    }
                }
            }

            return back()->withInput();
        } else if (\Auth::User()) {
            return redirect('/');
        } else {
            return redirect('login');
        }
    }


    public function deleteVariety(Request $request)
    {   
        if (!is_null($request->user()) && $request->user()->can('deleteVariety')) {
            $itemId = $request->id;
            if (!is_null($itemId)) {
                $variety = Variety::find($itemId);
                if ($variety) {
                    $variety->delete();
                }
            }

            return back();
        } else if (\Auth::User()) {
            return redirect('/');
        } else {
            return redirect('login');
        }
    }
}