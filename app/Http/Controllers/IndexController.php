<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Patent;
use App\Models\Variety;
use App\Models\Category;

class IndexController extends Controller
{
    public function index()
    {   //sss
        $posts = Post::all();
        if ($posts->count() < 4) {
           $posts = Post::orderBy('created_at', 'desc')->get()->random($posts->count());
        } else {
           $posts = Post::orderBy('created_at', 'desc')->get()->random(4); 
        }

        $varieties = Variety::limit(3)->orderBy('created_at', 'desc')->get();

    	return view('index', ['posts' => $posts, 'varieties' => $varieties]);
    }

    public function item(Request $request)
    {   
    	$post = Post::where('translit', $request->item_alias)->get();
        if ($post->count()) {
            return view('post', ['post' => $post]);
        } else {
            $variety = Variety::where('translit', $request->item_alias)->get();
            if ($variety->count()) {
                return view('variety', ['variety' => $variety]);
            }
        }

        return view('index', ['notInfo' => true]);
    }

    public function posts(Request $request)
    {   
        if ($request->category_alias) {
            $category = Category::where('translit', $request->category_alias)->get();
            $posts = Post::where('category_id', $category[0]->id)->orderBy('created_at', 'desc')->paginate(4);
            $request->session()->flash('category_title', $posts[0]->category->title);
        } else {
            if ($request->session()->has('category_title')) {
                $request->session()->forget('category_title');
            }
            $posts = Post::orderBy('created_at', 'desc')->paginate(4);
        }
    	
        if ($posts->count() < 4) {
           $randPosts = Post::orderBy('created_at', 'desc')->get()->random($posts->count());
        } else {
           $randPosts = Post::orderBy('created_at', 'desc')->get()->random(4); 
        }

        $categories = Category::all();

    	return view('posts', [
            'posts' => $posts, 
            'randPosts' => $randPosts, 
            'categories' => $categories
        ]);
    }

    public function patents(Request $request)
    {   
        $patents = Patent::orderBy('created_at', 'desc')->paginate(4);
        
        return view('patents', ['patents' => $patents]);
    }

    public function varieties(Request $request)
    {   
        $varieties = Variety::orderBy('created_at', 'desc')->paginate(4);

        return view('varieties', ['varieties' => $varieties]);
    }
}
