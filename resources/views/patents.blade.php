@extends('layouts.app')

@section('title')
  Патенты
@endsection
@section('meta_description')
  Страница содержащая для пользователя информацию о патентах
@endsection
@section('content')
<!-- ##### Breadcrumb Area Start ##### -->
  <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('/public/img/bg-img/18.jpg');">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcrumb-text">
            <h2>Патенты</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="famie-breadcrumb">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i> Главная</a></li>
          <li class="breadcrumb-item active" aria-current="page">Патенты</li>
        </ol>
      </nav>
    </div>
  </div>
  <!-- ##### Breadcrumb Area End ##### -->

  
    <!-- ##### News Details Area Start ##### -->
    <section class="news-details-area section-padding-0-100">
      <div class="container">
        @if (isset($patents) && $patents->count())
          <div class="row">
            <div class="col-12">
              <!-- News Details Content -->
              <div class="news-details-content">
                <table style="width:1110px;border:1px solid #f2f2f2;">
                  <tr>
                      <th style="padding:21px 10px 10px 40px;text-transform: uppercase;">Номер патента</th>
                      <th style="padding:21px 10px 10px 40px;text-transform: uppercase;">Название сорта</th>
                      <th style="padding:21px 10px 10px 40px;text-transform: uppercase;">Обладатель патента</th>
                      <th style="padding:21px 10px 10px 40px;text-transform: uppercase;">Дата регистрации</th>
                    </tr>
                  @foreach ($patents as $patent)
                    <tbody>
                      <tr style="border:1px solid #f2f2f2;">
                        <td style="padding:16px 10px 16px 40px;"><a href="">{!! $patent->registration_number !!}</a></td>
                        <td style="padding:16px 10px 16px 40px;">{!! $patent->variety->title !!}</td>
                        <td style="padding:16px 10px 16px 40px;">{!! $patent->owner !!}</td>
                        <td style="padding:16px 10px 16px 40px;">{!! $patent->patent_registration_date !!}</td>
                      </tr>
                    </tbody>
                  @endforeach
                </table>
              
              </div>      

            </div>
          </div>
        @else
          <div class="container" style="height: 800px; margin: 15% 0 0 -85%">
            <span style="color: #77b122"><b>Новости отсутствуют</b></span>
          </div>
        @endif
      </div>
    </section>
    <!-- ##### News Details Area End ##### -->
  @endsection