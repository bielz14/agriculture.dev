@extends('layouts.app')

@section('title')
  @if (isset($variety))
    {!! $variety[0]->meta_title !!}
  @else
    Сорт отсутствует
  @endif
@endsection
@section('meta_description')
  @if (isset($variety))
    {!! $variety[0]->meta_description !!}
  @endif
@endsection
@section('content')
  <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('/public/img/bg-img/18.jpg');">
      <div class="container h-100">
        <div class="row h-100 align-items-center">
          <div class="col-12">
            <div class="breadcrumb-text">
              <h2>Сорта</h2>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="famie-breadcrumb">
      <div class="container">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i> Главная</a></li>
            @if (isset($variety))
              <li class="breadcrumb-item active" aria-current="page">{!! $variety[0]->title !!}</li>
            @else
              <li class="breadcrumb-item active" aria-current="page">Новость отсутствует</li>
            @endif
          </ol>
        </nav>
      </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->

    @if (isset($variety))
      <!-- ##### News Details Area Start ##### -->
      <section class="news-details-area section-padding-0-100">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <!-- News Details Content -->
              <div class="news-details-content">
                <?php
                  $monthes = [
                    'Января',
                    'Февраля',
                    'Марта',
                    'Апреля',
                    'Мая',
                    'Июня',
                    'Июля',
                    'Августа',
                    'Сентября',
                    'Октября',
                    'Ноября',
                    'Декабря'
                  ];
                  $monthVarietyCreated = date('n', strtotime($variety[0]->created_at));
                  $month = $monthes[$monthVarietyCreated];
                  $day = date('d', strtotime($variety[0]->created_at));
                  $year = date('Y', strtotime($variety[0]->created_at));
                ?>
                <h6>Variety on {!! $day !!} {!! $month !!} {!! $year !!}</h6>
                <h2 class="variety-title">{!! $variety[0]->title !!}</h2>

                {!! $variety[0]->description !!}
                <div class="col-lg-8">
                    <img class="w-100 mb-30" src="/public/images/{!! $variety[0]->image !!}" alt="{!! $variety[0]->image_title !!}">
                </div>
              </div>      

            </div>
          </div>
        </div>
      </section>
  @else
    <span style="margin: 15% 0 0 25%; color: blue">Новость отсутствует</span>
  @endif
@endsection