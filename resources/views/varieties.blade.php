@extends('layouts.app')

@section('title')
  Сорта
@endsection
@section('meta_description')
  Страница с выводом списка сортов
@endsection
@section('content')
<!-- ##### Breadcrumb Area Start ##### -->
  <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('/public/img/bg-img/18.jpg');">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcrumb-text">
            <h2>Сорта</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="famie-breadcrumb">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i> Главная</a></li>
          <li class="breadcrumb-item active" aria-current="page">Сорта</li>
        </ol>
      </nav>
    </div>
  </div>
  <!-- ##### Breadcrumb Area End ##### -->

  <!-- ##### Blog Area Start ##### -->
  <section class="famie-blog-area">
    <div class="container">
      <div class="row">
        <!-- Varieties Area -->
        <div class="col-12 col-md-8">
          @if (isset($varieties) && $varieties->count())
            <div class="varieties-area">
                @foreach ($varieties as $variety)
                  <!-- Single Blog Variety Area -->
                  <div class="single-blog-variety-area mb-50 wow fadeInUp" data-wow-delay="100ms">
                    <?php
                      $monthes = [
                        'Января',
                        'Февраля',
                        'Марта',
                        'Апреля',
                        'Мая',
                        'Июня',
                        'Июля',
                        'Августа',
                        'Сентября',
                        'Октября',
                        'Ноября',
                        'Декабря'
                      ];
                      $monthVarietyCreated = date('n', strtotime($variety->created_at));
                      $month = $monthes[$monthVarietyCreated];
                      $day = date('d', strtotime($variety->created_at));
                      $year = date('Y', strtotime($variety->created_at));
                    ?>
                    <h6>Variety on <a href="#" class="variety-date">{!! $day !!} {!! $month !!} {!! $year !!}</a></h6>
                    <a href="/{!! $variety->translit !!}" class="variety-title">{!! $variety->title !!}</a>
                    <a href="/{!! $variety->translit !!}">
                      <img src="/public/images/{!! $variety->image !!}" alt="{!! $variety->image_title !!}" class="variety-thumb">
                    </a>
                    <p class="variety-excerpt">
                      {!! $variety->short_description !!}
                    </p>
                  </div>
                @endforeach
            </div>
            <nav>
              {{ $varieties->links('vendor.pagination.index') }}
            </nav>
          @else 
            <div class="container" style="height: 800px; margin: 15% 0 0 -85%">
              <span style="color: #77b122"><b>Сорта отсутствуют</b></span>
            </div>
          @endif
        </div>
      </div>
    </div>
  </section>
  <!-- ##### Blog Area End ##### -->
  @endsection