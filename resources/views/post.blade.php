@extends('layouts.app')

@section('title')
  @if (isset($post))
    {!! $post[0]->meta_title !!}
  @else
    Новость отсутствует
  @endif
@endsection
@section('meta_description')
  @if (isset($post))
    {!! $post[0]->meta_description !!}
  @endif
@endsection
@section('content')
  <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('/public/img/bg-img/18.jpg');">
      <div class="container h-100">
        <div class="row h-100 align-items-center">
          <div class="col-12">
            <div class="breadcrumb-text">
              <h2>Новости</h2>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="famie-breadcrumb">
      <div class="container">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i> Главная</a></li>
            @if (isset($post))
              <li class="breadcrumb-item active" aria-current="page">{!! $post[0]->title !!}</li>
            @else
              <li class="breadcrumb-item active" aria-current="page">Новость отсутствует</li>
            @endif
          </ol>
        </nav>
      </div>
    </div>
    <!-- ##### Breadcrumb Area End ##### -->

    @if (isset($post))
      <!-- ##### News Details Area Start ##### -->
      <section class="news-details-area section-padding-0-100">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <!-- News Details Content -->
              <div class="news-details-content">
                <?php
                  $monthes = [
                    'Января',
                    'Февраля',
                    'Марта',
                    'Апреля',
                    'Мая',
                    'Июня',
                    'Июля',
                    'Августа',
                    'Сентября',
                    'Октября',
                    'Ноября',
                    'Декабря'
                  ];
                  $monthPostCreated = date('n', strtotime($post[0]->created_at));
                  $month = $monthes[$monthPostCreated];
                  $day = date('d', strtotime($post[0]->created_at));
                  $year = date('Y', strtotime($post[0]->created_at));
                ?>
                <h6>Post on {!! $day !!} {!! $month !!} {!! $year !!}</h6>
                <h2 class="post-title">{!! $post[0]->title !!}</h2>

                {!! $post[0]->description !!}
                <div class="col-lg-8">
                    <img class="w-100 mb-30" src="/public/images/{!! $post[0]->image !!}" alt="{!! $post[0]->image_title !!}">
                </div>
              </div>      

            </div>
          </div>
        </div>
      </section>
  @else
    <span style="margin: 15% 0 0 25%; color: blue">Новость отсутствует</span>
  @endif
@endsection