@extends('layouts.app')

@section('title')
  @if(Session::has('category_title'))
    {!! Session::get('category_title') !!}
  @else
    Новости
  @endif
@endsection
@section('meta_description')
  @if(Session::has('category_title'))
    Новостная страница категории "{!! Session::get('category_title') !!}"
  @else
    Новостная страница
  @endif
@endsection
@section('content')
<!-- ##### Breadcrumb Area Start ##### -->
  <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('/public/img/bg-img/18.jpg');">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcrumb-text">
            <h2>Новости</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="famie-breadcrumb">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/"><i class="fa fa-home"></i> Главная</a></li>
          @if (!app('request')->category_alias)
            <li class="breadcrumb-item active">Новости</li>
          @else
            @if(Session::has('category_title'))
              <li class="breadcrumb-item" aria-current="page"><a href="/news">Новости</a></li>
              <li class="breadcrumb-item active" aria-current="page">{!! Session::get('category_title') !!}</li>
            @else
              <li class="breadcrumb-item active" aria-current="page">Новостная категория</li>
            @endif
          @endif
        </ol>
      </nav>
    </div>
    @if (isset($categories))
      <div class="container" style="margin-top: 2%">
        <nav aria-label="categories">
          @foreach ($categories as $category)
            <a style="padding: 0.5% 1.5% 0.5% 1.5%; background-color: #77b122; color: #ffffff;" href="/news/category/{!! $category->translit !!}">{!! $category->title !!}</a>
          @endforeach
        </nav>
      </div>
    @endif
  </div>
  <!-- ##### Breadcrumb Area End ##### -->

  <!-- ##### Blog Area Start ##### -->
  <section class="famie-blog-area">
    <div class="container">
      <div class="row">
        <!-- Posts Area -->
        <div class="col-12 col-md-8">
          @if (isset($posts) && $posts->count())
            <div class="posts-area">
                @foreach ($posts as $post)
                  <!-- Single Blog Post Area -->
                  <div class="single-blog-post-area mb-50 wow fadeInUp" data-wow-delay="100ms">
                    <?php
                      $monthes = [
                        'Января',
                        'Февраля',
                        'Марта',
                        'Апреля',
                        'Мая',
                        'Июня',
                        'Июля',
                        'Августа',
                        'Сентября',
                        'Октября',
                        'Ноября',
                        'Декабря'
                      ];
                      $monthPostCreated = date('n', strtotime($post->created_at));
                      $month = $monthes[$monthPostCreated];
                      $day = date('d', strtotime($post->created_at));
                      $year = date('Y', strtotime($post->created_at));
                    ?>
                    <h6>Post on <a href="#" class="post-date">{!! $day !!} {!! $month !!} {!! $year !!}</a></h6>
                    <a href="/{!! $post->translit !!}" class="post-title">{!! $post->title !!}</a>
                    <a href="/{!! $post->translit !!}">
                      <img src="/public/images/{!! $post->image !!}" alt="{!! $post->image_title !!}" class="post-thumb">
                    </a>
                    <p class="post-excerpt">
                      {!! $post->short_description !!}
                    </p>
                  </div>
                @endforeach
            </div>
            <nav>
              {{ $posts->links('vendor.pagination.index') }}
            </nav>
          @endif
        </div>

        <!-- Sidebar Area -->
        <div class="col-12 col-md-4">
          <div class="sidebar-area">

        
            <!-- Single Widget Area -->
            <div class="single-widget-area">
              @if (isset($randPosts) && $randPosts->count())
              <!-- Title -->
              <h5 class="widget-title">Recent News</h5>
                @foreach ($randPosts as $randPost)
                  <!-- Single Recent News Start -->
                  <div class="single-recent-blog style-2 d-flex align-items-center">
                    <div class="post-thumbnail">
                      <a href="/{!! $randPost->translit !!}">
                        <img src="/public/images/{!! $randPost->image !!}" alt="">
                      </a>
                    </div>
                    <div class="post-content">
                      <a href="/{!! $randPost->translit !!}" class="post-title">{!! $randPost->title !!}</a>
                      <?php
                        $monthes = [
                          'Января',
                          'Февраля',
                          'Марта',
                          'Апреля',
                          'Мая',
                          'Июня',
                          'Июля',
                          'Августа',
                          'Сентября',
                          'Октября',
                          'Ноября',
                          'Декабря'
                        ];
                        $monthPostCreated = date('n', strtotime($randPost->created_at));
                        $month = $monthes[$monthPostCreated];
                        $day = date('d', strtotime($randPost->created_at));
                        $year = date('Y', strtotime($randPost->created_at));
                      ?>
                      <div class="post-date">{!! $day !!} {!! $month !!} {!! $year !!}</div>
                    </div>
                  </div>
                @endforeach
              @else
                <div class="container" style="height: 800px; margin: 15% 0 0 -85%">
                  <span style="color: #77b122"><b>Новости отсутствуют</b></span>
                </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ##### Blog Area End ##### -->
  @endsection