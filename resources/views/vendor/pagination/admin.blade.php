@if ($paginator->hasPages())
    <ul class="paginator iblock" id="yw0">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="disabled first"><span>«</span></li>
        @else
            <li clas="first"><a href="{{ $paginator->previousPageUrl() }}" rel="prev">«</a></li>
        @endif


        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled page"><span>{{ $element }}</span></li>
            @endif


            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page selected"><span>{{ $page }}</span></li>
                    @else
                        <li class="page"><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach


        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="next"><a href="{{ $paginator->nextPageUrl() }}" rel="next">»</a></li>
        @else
            <li class="disabled next"><span>»</span></li>
        @endif
    </ul>
@endif
