@extends('layouts.app')

@section('title')
  Главная
@endsection
@section('meta_description')
  Главная страница сельскохозяйственного сайта
@endsection
@section('content')
  @if (isset($notInfo) && $notInfo)
    <div class="container" style="height: 800px; margin: 15% 0 0 -85%">
      <span style="color: #77b122"><b>Информация отсутствует</b></span>
    </div>
  @else
    <!-- ##### Famie Benefits Area Start ##### -->
    <section class="famie-benefits-area section-padding-100-0 pb-5">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="benefits-thumbnail mb-50">
              <img src="/public/img/bg-img/2.jpg" alt="">
            </div>
          </div>
        </div>

        <div class="row justify-content-center">
          <!-- Single Benefits Area -->
          <div class="col-12 col-sm-4 col-lg">
            <div class="single-benefits-area wow fadeInUp mb-50" data-wow-delay="100ms">
              <img src="/public/img/core-img/digger.png" alt="">
              <h5>Лучший сервис</h5>
            </div>
          </div>

          <!-- Single Benefits Area -->
          <div class="col-12 col-sm-4 col-lg">
            <div class="single-benefits-area wow fadeInUp mb-50" data-wow-delay="300ms">
              <img src="/public/img/core-img/windmill.png" alt="">
              <h5>Фермерский опыт</h5>
            </div>
          </div>

          <!-- Single Benefits Area -->
          <div class="col-12 col-sm-4 col-lg">
            <div class="single-benefits-area wow fadeInUp mb-50" data-wow-delay="500ms">
              <img src="/public/img/core-img/cereals.png" alt="">
              <h5>100% натуральные</h5>
            </div>
          </div>

          <!-- Single Benefits Area -->
          <div class="col-12 col-sm-4 col-lg">
            <div class="single-benefits-area wow fadeInUp mb-50" data-wow-delay="700ms">
              <img src="/public/img/core-img/tractor.png" alt="">
              <h5>Фермерское оборудование</h5>
            </div>
          </div>

          <!-- Single Benefits Area -->
          <div class="col-12 col-sm-4 col-lg">
            <div class="single-benefits-area wow fadeInUp mb-50" data-wow-delay="900ms">
              <img src="/public/img/core-img/sunrise.png" alt="">
              <h5>Органическая еда</h5>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- ##### Famie Benefits Area End ##### -->

    <!-- ##### About Us Area Start ##### -->
    <section class="about-us-area">
      <div class="container">
        <div class="row align-items-center">

          <!-- About Us Content -->
          <div class="col-12 col-md-8">
            <div class="about-us-content mb-100">
              <!-- Section Heading -->
              <div class="section-heading">
                <p>О нашей компании</p>
                <h2><span>Позвольте</span> рассказать о нашей компании</h2>
                <img src="/public/img/core-img/decor.png" alt="">
              </div>
              <p>Lorem ipsum dolor sit amet, consectetu adipiscing elit. Etiam nunc elit, pretium atlanta urna veloci, fermentum malesuda mina. Donec auctor nislec neque sagittis, sit amet dapibus pellentesque donal feugiat. Nulla mollis magna non
                sanaliquet, volutpat do zutum, ultrices consectetur, ultrices at purus.</p>
              <a href="#" class="btn famie-btn mt-30">Read More</a>
            </div>
          </div>

          <!-- Famie Video Play -->
          <div class="col-12 col-md-4">
            <div class="famie-video-play mb-100">
              <img src="/public/img/bg-img/6.jpg" alt="">
            </div>
          </div>

        </div>
      </div>
    </section>
    <!-- ##### About Us Area End ##### -->

    <!-- ##### Services Area Start ##### -->
    <section class="services-area d-flex flex-wrap">
      <!-- Service Thumbnail -->
      <div class="services-thumbnail bg-img jarallax" style="background-image: url('img/bg-img/7.jpg');"></div>

      <!-- Service Content -->
      <div class="services-content section-padding-100-50 px-5">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <!-- Section Heading -->
              <div class="section-heading">
                <p>Что мы производим</p>
                <h2><span>Наша продукция</span> - лучшая</h2>
                <img src="/public/img/core-img/decor.png" alt="">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-12 mb-50">
              <p>Mauris fermentum nunc quis massa lacinia consequat. Suspendisse orci magna, pharetra sedonia risus ut,
                elementum mollis nisin. Nunc in sapien turpis. Donec egeto david orci pulvinar ultrices necto drax turpis.
                Pellentesque justo metus, semper nec ullamcorper id, gravida ultricies arcu.</p>
            </div>

            <!-- Single Service Area -->
            <div class="col-12 col-lg-6">
              <div class="single-service-area mb-50 wow fadeInUp" data-wow-delay="100ms">
                <!-- Service Title -->
                <div class="service-title mb-3 d-flex align-items-center">
                  <img src="/public/img/core-img/s1.png" alt="">
                  <h5>Фрукты &amp; Овощи</h5>
                </div>
                <p>Intiam eu sagittis est, aster cosmo lacini libero. Praesent dignissim sed odio velo aliquam manta legolas. </p>
              </div>
            </div>

            <!-- Single Service Area -->
            <div class="col-12 col-lg-6">
              <div class="single-service-area mb-50 wow fadeInUp" data-wow-delay="300ms">
                <!-- Service Title -->
                <div class="service-title mb-3 d-flex align-items-center">
                  <img src="/public/img/core-img/s2.png" alt="">
                  <h5>Мясо &amp; Яйца</h5>
                </div>
                <p>Intiam eu sagittis est, aster cosmo lacini libero. Praesent dignissim sed odio velo aliquam manta legolas. </p>
              </div>
            </div>

            <!-- Single Service Area -->
            <div class="col-12 col-lg-6">
              <div class="single-service-area mb-50 wow fadeInUp" data-wow-delay="500ms">
                <!-- Service Title -->
                <div class="service-title mb-3 d-flex align-items-center">
                  <img src="/public/img/core-img/s3.png" alt="">
                  <h5>Молоко &amp; Сыр</h5>
                </div>
                <p>Intiam eu sagittis est, aster cosmo lacini libero. Praesent dignissim sed odio velo aliquam manta legolas. </p>
              </div>
            </div>

            <!-- Single Service Area -->
            <div class="col-12 col-lg-6">
              <div class="single-service-area mb-50 wow fadeInUp" data-wow-delay="700ms">
                <!-- Service Title -->
                <div class="service-title mb-3 d-flex align-items-center">
                  <img src="/public/img/core-img/s4.png" alt="">
                  <h5>Пшеница &amp; Кукуруза</h5>
                </div>
                <p>Intiam eu sagittis est, aster cosmo lacini libero. Praesent dignissim sed odio velo aliquam manta legolas. </p>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
    <!-- ##### Services Area End ##### -->

    <!-- ##### Farming Practice Area Start ##### -->
    <section class="farming-practice-area section-padding-100-50">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <!-- Section Heading -->
            <div class="section-heading text-center">
              <h2>Сорта</h2>
              <img src="/public/img/core-img/decor2.png" alt="">
            </div>
          </div>
        </div>

        <div class="row">
          @if (isset($varieties) && $varieties->count())
            @foreach ($varieties as $variety)
              <!-- Single Farming Practice Area -->
              <div class="col-12 col-sm-6 col-lg-4">
                <div class="single-farming-practice-area mb-50 wow fadeInUp" data-wow-delay="100ms">
                  <!-- Thumbnail -->
                  <div class="farming-practice-thumbnail">
                    <img src="/public/images/{!! $variety->image !!}" alt="">
                  </div>
                  <!-- Content -->
                  <div class="farming-practice-content text-center">
                    <!-- Icon -->
                    <h4><a href="/{!! $variety->translit !!}">{!! $variety->title !!}</a></h4>
                    <p>{!! $variety->short_description !!}</p>
                  </div>
                </div>
              </div>
            @endforeach
          @endif
        </div>
      </div>
    </section>
    <!-- ##### Farming Practice Area End ##### -->

    <!-- ##### News Area Start ##### -->
    <section class="news-area bg-gray section-padding-100-0">
      <div class="container">
        <div class="row">

          <!-- Featured Post Area -->
          <div class="col-12 col-lg-6">
            <div class="featured-post-area mb-100 wow fadeInUp" data-wow-delay="100ms">
              <img src="/public/img/bg-img/17.jpg" alt="">
              <!-- Post Content -->
              <div class="post-content">
                <a href="#" class="post-title">Новости компании</a>
              </div>
            </div>
          </div>

          <!-- Single Blog Area -->
          <div class="col-12 col-lg-6 mb-100">
            @if (isset($posts) && $posts->count())
              @foreach ($posts as $post)
                <!-- Single Blog Area -->
                <div class="single-blog-area style-2 wow fadeInUp" data-wow-delay="300ms">
                  <!-- Post Content -->
                  <div class="post-content">
                    <?php
                      $monthes = [
                        'Января',
                        'Февраля',
                        'Марта',
                        'Апреля',
                        'Мая',
                        'Июня',
                        'Июля',
                        'Августа',
                        'Сентября',
                        'Октября',
                        'Ноября',
                        'Декабря'
                      ];
                      $monthPostCreated = date('n', strtotime($post->created_at));
                      $month = $monthes[$monthPostCreated];
                      $day = date('d', strtotime($post->created_at));
                      $year = date('Y', strtotime($post->created_at));
                    ?>
                    <h6>{!! $day !!} {!! $month !!} {!! $year !!}</h6>
                    <a href="/{!! $post->translit !!}" class="post-title">{!! $post->title !!}</a>
                    <p>{!! $post->short_description !!}</p>
                  </div>
                </div>
              @endforeach
            @endif
          </div>
        </div>
      </div>
    </section>
    <!-- ##### News Area End ##### -->
  @endif
  @endsection