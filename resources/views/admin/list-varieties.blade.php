   <div class="row-fluid list" style="display: inline-block">
      <div class="grid-view">
            @if (isset($varieties) && $varieties->count())
               <div class="summary">
                  <div class="table-header" style="background-color:#f9f9f9;color:#666;">
                     Всего {!! $varieties->total() !!}
                  </div>
               </div>
               <table class="table table-striped table-bordered table-hover">
                  <thead>
                     <tr>
                        <th><a class="csorting" href="/">ID</a></th>
                        <th>URL</th>
                        <th>Заголовок</th>
                        <th>МЕТА: описание</th>
                        <th>Edit</th>
                        <th>Del</th>
                        <th>View</th>
                     </tr>
                  </thead>
                  <tbody id="admin-tbody">
                     @foreach ($varieties as $key => $variety)
                        <tr class="edit">
                           <td>{!! $key + 1 !!}</td>
                           <td>{!! url('/'. $variety->translit) !!}</td>
                           <td>{!! $variety->title !!}</td>
                           <td>{!! $variety->meta_description !!}</td>
                           <td>
                              <a class="form" href="/admin/varieties?id={!! $variety->id !!}">Edit</a>    
                           </td>
                           <td>
                              {{ Form::open(array('url' => '/admin/delete-variety', 'method' => 'variety', 'id' => 'delete-form')) }}
                                 {!! Form::hidden('id', $variety->id, ['id' => 'itemId']) !!}
                                 {!! 
                                    Form::submit('Del', 
                                       [
                                       'style' => 'background:none;
                                                   border:none; 
                                                   padding:0!important;
                                                   font: inherit;
                                                   /*border is optional*/
                                                   cursor: pointer;
                                                   color: #08c;',
                                       ]
                                    ); 
                                 !!}
                              {{ Form::close() }}   
                           </td>
                           <td><a class="view" href="/{!! $variety->translit !!}">View</a>    </td>
                        </tr>
                     @endforeach
                  </tbody>
               </table>
        
               <div class="pagination row" style="margin: 5% 0 0 43%">
                  {{ $varieties->links('vendor.pagination.admin') }}
               </div>
            @else
               <div>
                  <span>Сорта отсутствуют</span>
               </div>
            @endif
      </div>
   </div>