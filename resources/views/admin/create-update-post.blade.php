   <div class="row-fluid create-update" id="form-ajax" style="display: none">
      @if (!isset($item))
        <div id="create">
          {{ Form::open(array('url' => '/admin/create-post', 'method' => 'post', 'id' => 'create-form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
              <div class="control-group">
                 <label class="control-label required" for="AdminNews_title">Заголовок <span class="required">*</span></label>            
                 <div class="controls">
                      {!! Form::text('title', '', ['id' => 'AdminNews_title', 'class' => 'span12', 'size' => '60', 'maxlength' => 255]) !!}
                      @if ($errors->has('title'))
                          <span class="help-block">
                              <strong>{!! $errors->first('title') !!}</strong>
                          </span>
                      @endif
                 </div>
              </div>
              <div class="control-group">
                 <label class="control-label" for="AdminPrice_service_id">Категория</label>            
                 <div class="controls">
                     <select class="span12" name="category_id" id="AdminPrice_service_id">
                     @if (isset($categories))
                         @foreach ($categories as $category)
                              <option value="{!! $category->id !!}">{!! $category->title !!}</option>
                         @endforeach   
                     @else
                        <option value="1">Агрокультуры</option> 
                        <option value="2">Злаковые культуры</option> 
                     @endif
                    </select>

                    <div class="error">
                          @if ($errors->has('category_id'))
                              <span class="help-block">
                                  <strong>{!! $errors->first('title') !!}</strong>
                              </span>
                          @endif
                    </div>
                 </div>
              </div>
              <div class="control-group">
                 <label class="control-label" for="AdminNews_description">Краткое описание</label>            
                 <div class="controls">
                    {!! Form::textarea('short_description', NULL, ['cols' => '20', 'rows' => '12', 'id' => 'AdminNews_description', 'class' => 'txt span12', 'placeholder' => 'ru']) !!}
                      <div class="error">
                          @if ($errors->has('short_description'))
                              <span class="help-block">
                                  <strong>{!! $errors->first('short-description') !!}</strong>
                              </span>
                          @endif
                      </div>
                 </div>
              </div>
              <div class="control-group">
                 <div class="span12">
                    <div class="trumbowyg-box trumbowyg-editor-visible trumbowyg-en trumbowyg">
                        {!! Form::textarea('description', NULL, ['id' => 'trumbowyg-demo', 'class' => 'txt span12', 'placeholder' => 'Описание', 'tabindex' => '-1', 'style' => 'height: 57px; overflow: auto;']) !!}
                    </div>
                 </div>
                 <div class="error">
                      @if ($errors->has('description'))
                          <span class="help-block">
                              <strong>{!! $errors->first('description') !!}</strong>
                          </span>
                      @endif   
                </div>
              </div>
              <div class="control-group">
                 <label for="" class="control-label">
                 <label for="AdminNews_image">Изображение</label>            </label>


                 <div class="controls">
                    {!! Form::file('image', ['id' => 'id-input-filed', 'onchange' => 'previewFile()']) !!}
                      @if ($errors->has('image'))
                        <span class="help-block">
                           <strong>{!! $errors->first('image') !!}</strong>
                        </span>
                     @endif
                 </div>
                 <img id="poster" src="" alt="Постер..." style="display: none; margin-left: 2.5%; height: 80px">
              </div>
              <div class="control-group">
                 <label class="control-label" for="AdminNews_image_alt">Название изображения</label>            
                 <div class="controls">
                      {!! Form::text('image_title', '', ['id' => 'AdminNews_image_alt', 'class' => 'span12', 'size' => '60', 'maxlength' => 255]) !!}            
                    <div class="error">
                      @if ($errors->has('image_title'))
                          <span class="help-block">
                              <strong>{!! $errors->first('image_title') !!}</strong>
                          </span>
                      @endif   
                    </div>
                 </div>
              </div>
              <div class="control-group">
                 <label class="control-label" for="AdminNews_meta_title">Мета заголовок</label>            
                 <div class="controls">                
                    {!! Form::text('meta_title', '', ['id' => 'AdminNews_meta_title', 'class' => 'span12', 'size' => '60', 'maxlength' => 255]) !!}            
                    <div class="error">
                      @if ($errors->has('meta_title'))
                          <span class="help-block">
                              <strong>{!! $errors->first('meta_title') !!}</strong>
                          </span>
                      @endif  
                    </div>
              </div><br>
              <div class="control-group">
                 <label class="control-label" for="AdminNews_meta_description">Мета описание</label>            
                 <div class="controls">
                   {!! Form::textarea('meta_description', NULL, ['id' => 'trumbowyg-demo', 'class' => 'txt span12', 'col' => '50', 'rows' => '12', 'placeholder' => 'ru']) !!}
                    <div class="error">
                      @if ($errors->has('meta_description'))
                          <span class="help-block">
                              <strong>{!! $errors->first('meta_description') !!}</strong>
                          </span>
                      @endif  
                    </div>
                 </div>
              </div>
              <div class="control-group">
                 <label class="control-label" for="AdminNews_translit">Транслит</label>            
                 <div class="controls">   
                    {!! Form::text('translit', '', ['id' => 'AdminNews_translit', 'class' => 'span12', 'size' => '60', 'maxlength' => 255]) !!}            
                    <div class="error">
                      @if ($errors->has('translit'))
                          <span class="help-block">
                              <strong>{!! $errors->first('translit') !!}</strong>
                          </span>
                      @endif   
                    </div>
                 </div>
              </div>
              <div class="form-actions">
                  {!! Form::submit('Сохранить', ['name' => 'yt0', 'class' => 'btn btn-info']); !!}
                  {!! Form::input('reset', null, 'Отмена', ['value' => 'Отмена', 'name' => 'yt1', 'class' => 'btn']) !!}      
              </div>
          {{ Form::close() }}
        </div>
      @else
        <div id="update">
          {{ Form::open(array('url' => '/admin/update-post', 'method' => 'post', 'id' => 'update-form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
              {!! Form::hidden('itemId', $item->id, ['id' => 'itemId']) !!}
              <div class="control-group">
                 <label class="control-label required" for="AdminNews_title">Заголовок <span class="required">*</span></label>            
                 <div class="controls">
                      {!! Form::text('title', $item->title, ['id' => 'AdminNews_title', 'class' => 'span12', 'size' => '60', 'maxlength' => 255]) !!}
                      @if ($errors->has('title'))
                          <span class="help-block">
                              <strong>{!! $errors->first('title') !!}</strong>
                          </span>
                      @endif
                 </div>
              </div>
              <div class="control-group">
                 <label class="control-label" for="AdminPrice_service_id">Категория</label>            
                 <div class="controls">
                     <select class="span12" name="category_id" id="AdminPrice_service_id">
                     @if (isset($categories))
                         @foreach ($categories as $category)
                              @if ($item->category_id == $category->id)
                                <option value="{!! $category->id !!}" selected="selected">{!! $category->title !!}</option>
                              @else
                                <option value="{!! $category->id !!}">{!! $category->title !!}</option>
                              @endif
                         @endforeach   
                     @else
                        <option value="1">Агрокультуры</option> 
                        <option value="2">Злаковые культуры</option> 
                     @endif
                    </select>

                    <div class="error">
                          @if ($errors->has('category_id'))
                              <span class="help-block">
                                  <strong>{!! $errors->first('title') !!}</strong>
                              </span>
                          @endif
                    </div>
                 </div>
              </div>
              <div class="control-group">
                 <label class="control-label" for="AdminNews_description">Краткое описание</label>            
                 <div class="controls">
                    {!! Form::textarea('short_description', $item->short_description, ['cols' => '20', 'rows' => '12', 'id' => 'AdminNews_description', 'class' => 'txt span12', 'placeholder' => 'ru']) !!}
                      <div class="error">
                          @if ($errors->has('short_description'))
                              <span class="help-block">
                                  <strong>{!! $errors->first('short-description') !!}</strong>
                              </span>
                          @endif
                      </div>
                 </div>
              </div>
              <div class="control-group">
                 <div class="span12">
                    <div class="trumbowyg-box trumbowyg-editor-visible trumbowyg-en trumbowyg">
                        {!! Form::textarea('description', $item->description, ['id' => 'trumbowyg-demo', 'class' => 'txt span12', 'placeholder' => 'Описание', 'tabindex' => '-1', 'style' => 'height: 57px; overflow: auto;']) !!}
                    </div>
                 </div>
                 <div class="error">
                      @if ($errors->has('description'))
                          <span class="help-block">
                              <strong>{!! $errors->first('description') !!}</strong>
                          </span>
                      @endif   
                </div>
              </div>
              <div class="control-group">
                 <label for="" class="control-label">
                 <label for="AdminNews_image">Изображение</label>            </label>


                 <div class="controls">
                    {!! Form::file('image', ['id' => 'id-input-filed', 'onchange' => 'previewFile()']) !!}
                      @if ($errors->has('image'))
                        <span class="help-block">
                           <strong>{!! $errors->first('image') !!}</strong>
                        </span>
                     @endif
                 </div>
                 @if (!is_null($item->image) && $item->image !== '')
                    <img id="poster" src="/public/images/{!! $item->image !!}" style="margin-left: 2.5%; height: 80px" alt="Постер...">
                 @else
                    <img id="poster" src="" style="height: 80px" alt="Постер..." style="display: none; margin-left: 2.5%; height: 80px">
                 @endif
              </div>
              <div class="control-group">
                 <label class="control-label" for="AdminNews_image_alt">Название изображения</label>            
                 <div class="controls">
                      {!! Form::text('image_title', $item->image_title, ['id' => 'AdminNews_image_alt', 'class' => 'span12', 'size' => '60', 'maxlength' => 255]) !!}            
                    <div class="error">
                      @if ($errors->has('image_title'))
                          <span class="help-block">
                              <strong>{!! $errors->first('image_title') !!}</strong>
                          </span>
                      @endif   
                    </div>
                 </div>
              </div>
              <div class="control-group">
                 <label class="control-label" for="AdminNews_meta_title">Мета заголовок</label>            
                 <div class="controls">                
                    {!! Form::text('meta_title', $item->meta_title, ['id' => 'AdminNews_meta_title', 'class' => 'span12', 'size' => '60', 'maxlength' => 255]) !!}            
                    <div class="error">
                      @if ($errors->has('meta_title'))
                          <span class="help-block">
                              <strong>{!! $errors->first('meta_title') !!}</strong>
                          </span>
                      @endif  
                    </div>
              </div><br>
              <div class="control-group">
                 <label class="control-label" for="AdminNews_meta_description">Мета описание</label>            
                 <div class="controls">
                   {!! Form::textarea('meta_description', $item->meta_description, ['id' => 'trumbowyg-demo', 'class' => 'txt span12', 'col' => '50', 'rows' => '12', 'placeholder' => 'ru']) !!}
                    <div class="error">
                      @if ($errors->has('meta_description'))
                          <span class="help-block">
                              <strong>{!! $errors->first('meta_description') !!}</strong>
                          </span>
                      @endif  
                    </div>
                 </div>
              </div>
              <div class="control-group">
                 <label class="control-label" for="AdminNews_translit">Транслит</label>            
                 <div class="controls">   
                    {!! Form::text('translit', $item->translit, ['id' => 'AdminNews_translit', 'class' => 'span12', 'size' => '60', 'maxlength' => 255]) !!}            
                    <div class="error">
                      @if ($errors->has('translit'))
                          <span class="help-block">
                              <strong>{!! $errors->first('translit') !!}</strong>
                          </span>
                      @endif   
                    </div>
                 </div>
              </div>
              <div class="form-actions">
                  {!! Form::submit('Обновить', ['name' => 'yt0', 'class' => 'btn btn-info']); !!}
                  {!! Form::input('reset', null, 'Отмена', ['value' => 'Отмена', 'name' => 'yt1', 'class' => 'btn']) !!}      
              </div>
          {{ Form::close() }}
        </div>
      @endif;
   </div>
   <script>      
    function previewFile() {
      var preview = document.querySelector('img#poster');
      var file    = document.querySelector('input[type=file]').files[0];
      var reader  = new FileReader();
      reader.onloadend = function () {
          preview.src = reader.result;
      }

      if (file) {
        reader.readAsDataURL(file);
        if (!$('#poster').is(':visible')) {
            $('#poster').toggle();
        }
      } else {
        review.src = "";
        if ($('#poster').is(':visible')) {
            $('#poster').toggle();
        }
      }
    }          
  </script>