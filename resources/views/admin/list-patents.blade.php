   <div class="row-fluid list" style="display: inline-block">
      <div class="grid-view">
            @if (isset($patents) && $patents->count())
               <div class="summary">
                  <div class="table-header" style="background-color:#f9f9f9;color:#666;">
                     Всего {!! $patents->total() !!}
                  </div>
               </div>
               <table class="table table-striped table-bordered table-hover">
                  <thead>
                     <tr>
                        <th><a class="csorting" href="/">ID</a></th>
                        <th>Регистрационный номер</th>
                        <th>Обладатель патента</th>
                        <th>Дата регистрации патента</th>
                        <th>Сорт</th>
                        <th>Edit</th>
                        <th>Del</th>
                     </tr>
                  </thead>
                  <tbody id="admin-tbody">
                     @foreach ($patents as $key => $patent)
                        <tr class="edit">
                           <td>{!! $key + 1 !!}</td>
                           <td>{!! $patent->registration_number !!}</td>
                           <td>{!! $patent->owner !!}</td>
                           <td>{!! $patent->patent_registration_date !!}</td>
                           <td>{!! $patent->variety->title !!}</td>
                           <td>
                              <a class="form" href="/admin/patents?id={!! $patent->id !!}">Edit</a>    
                           </td>
                           <td>
                              {{ Form::open(array('url' => '/admin/delete-patent', 'method' => 'patent', 'id' => 'delete-form')) }}
                                 {!! Form::hidden('id', $patent->id, ['id' => 'itemId']) !!}
                                 {!! 
                                    Form::submit('Del', 
                                       [
                                       'style' => 'background:none;
                                                   border:none; 
                                                   padding:0!important;
                                                   font: inherit;
                                                   /*border is optional*/
                                                   cursor: pointer;
                                                   color: #08c;',
                                       ]
                                    ); 
                                 !!}
                              {{ Form::close() }}   
                           </td>
                        </tr>
                     @endforeach
                  </tbody>
               </table>
        
               <div class="pagination row" style="margin: 5% 0 0 43%">
                  {{ $patents->links('vendor.pagination.admin') }}
               </div>
            @else
               <div>
                  <span>Патенты отсутствуют</span>
               </div>
            @endif
      </div>
   </div>