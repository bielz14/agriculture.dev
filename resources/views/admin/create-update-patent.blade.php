   <div class="row-fluid create-update" id="form-ajax" style="display: none">
      @if (!isset($item))
        <div id="create">
          {{ Form::open(array('url' => '/admin/create-patent', 'method' => 'post', 'id' => 'create-form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
              <div class="control-group">
                 <label class="control-label required" for="AdminPatent_reg_num">Номер регистрации <span class="required">*</span></label>            
                 <div class="controls">
                      {!! Form::text('registration_number', '', ['id' => 'AdminPatent_reg_num', 'class' => 'span12', 'size' => '60', 'maxlength' => 10]) !!}
                      @if ($errors->has('registration_number'))
                          <span class="help-block">
                              <strong>{!! $errors->first('registration_number') !!}</strong>
                          </span>
                      @endif
                 </div>
              </div>
              <div class="control-group">
                 <label class="control-label required" for="AdminPatent_reg_date">Дата регистрации <span class="required">*</span></label>            
                 <div class="controls">
                      {!! Form::date('patent_registration_date', '', ['id' => 'AdminPatent_reg_date', 'class' => 'span12', 'size' => '60', 'maxlength' => 10]) !!}
                      @if ($errors->has('patent_registration_date'))
                          <span class="help-block">
                              <strong>{!! $errors->first('patent_registration_date') !!}</strong>
                          </span>
                      @endif
                 </div>
              </div>
              <div class="control-group">
                 <label class="control-label required" for="AdminPatent_owner">Обладатель патента <span class="required">*</span></label>            
                 <div class="controls">
                      {!! Form::text('owner', '', ['id' => 'AdminPatent_reg_date', 'class' => 'span12', 'size' => '60']) !!}
                      @if ($errors->has('owner'))
                          <span class="help-block">
                              <strong>{!! $errors->first('owner') !!}</strong>
                          </span>
                      @endif
                 </div>
              </div>
              <div class="control-group">
                 <label class="control-label" for="AdminPrice_category">Категория</label>            
                 <div class="controls">
                     <select class="span12" name="variety_id" id="AdminPrice_category">
                     @if (isset($varieties))
                         @foreach ($varieties as $variety)
                              <option value="{!! $variety->id !!}">{!! $variety->title !!}</option>
                         @endforeach   
                     @else
                        <option value="1">Агрокультуры</option> 
                        <option value="2">Злаковые культуры</option> 
                     @endif
                    </select>

                    <div class="error">
                          @if ($errors->has('variety_id'))
                              <span class="help-block">
                                  <strong>{!! $errors->first('title') !!}</strong>
                              </span>
                          @endif
                    </div>
                 </div>
              </div>
              <div class="control-group">
                 <label for="" class="control-label">
                 <label for="AdminPatent_image">Изображение</label>            </label>
                 <div class="controls">
                    {!! Form::file('image', ['id' => 'id-input-filed', 'onchange' => 'previewFile()']) !!}
                      @if ($errors->has('image'))
                        <span class="help-block">
                           <strong>{!! $errors->first('image') !!}</strong>
                        </span>
                     @endif
                 </div>
                 <img id="poster" src="" alt="Постер..." style="display: none; margin-left: 2.5%; height: 80px">
              </div>
              <div class="form-actions">
                  {!! Form::submit('Сохранить', ['name' => 'yt0', 'class' => 'btn btn-info']); !!}
                  {!! Form::input('reset', null, 'Отмена', ['value' => 'Отмена', 'name' => 'yt1', 'class' => 'btn']) !!}      
              </div>
          {{ Form::close() }}
        </div>
      @else
        <div id="update">
          {{ Form::open(array('url' => '/admin/update-patent', 'method' => 'post', 'id' => 'update-form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
              {!! Form::hidden('itemId', $item->id, ['id' => 'itemId']) !!}
              <div class="control-group">
                 <label class="control-label required" for="AdminPatent_title">Номер регистрации <span class="required">*</span></label>            
                 <div class="controls">
                      {!! Form::text('registration_number', $item->registration_number, ['id' => 'AdminPatent_title', 'class' => 'span12', 'size' => '60', 'maxlength' => 10]) !!}
                      @if ($errors->has('registration_number'))
                          <span class="help-block">
                              <strong>{!! $errors->first('registration_number') !!}</strong>
                          </span>
                      @endif
                 </div>
              </div>
              <div class="control-group">
                 <label class="control-label required" for="AdminPatent_reg_date">Дата регистрации <span class="required">*</span></label>            
                 <div class="controls">
                      {!! Form::date('patent_registration_date', $item->patent_registration_date, ['id' => 'AdminPatent_reg_date', 'class' => 'span12', 'size' => '60', 'maxlength' => 10]) !!}
                      @if ($errors->has('patent_registration_date'))
                          <span class="help-block">
                              <strong>{!! $errors->first('patent_registration_date') !!}</strong>
                          </span>
                      @endif
                 </div>
              </div>
              <div class="control-group">
                 <label class="control-label required" for="AdminPatent_owner">Обладатель патента <span class="required">*</span></label>            
                 <div class="controls">
                      {!! Form::text('owner', $item->owner, ['id' => 'AdminPatent_reg_date', 'class' => 'span12', 'size' => '60']) !!}
                      @if ($errors->has('owner'))
                          <span class="help-block">
                              <strong>{!! $errors->first('owner') !!}</strong>
                          </span>
                      @endif
                 </div>
              </div>
              <div class="control-group">
                 <label class="control-label" for="AdminPrice_service_id">Категория</label>         
                 <div class="controls">
                     <select class="span12" name="variety_id" id="AdminPrice_service_id">
                     @if (isset($varieties))
                         @foreach ($varieties as $variety)
                              @if ($item->variety_id == $variety->id)
                                <option value="{!! $variety->id !!}" selected="selected">{!! $variety->title !!}</option>
                              @else
                                <option value="{!! $variety->id !!}">{!! $variety->title !!}</option>
                              @endif
                         @endforeach   
                     @else
                        <option value="1">Агрокультуры</option> 
                        <option value="2">Злаковые культуры</option> 
                     @endif
                    </select>

                    <div class="error">
                          @if ($errors->has('variety_id'))
                              <span class="help-block">
                                  <strong>{!! $errors->first('title') !!}</strong>
                              </span>
                          @endif
                    </div>
                 </div>
              </div>
              <div class="control-group">
                 <label for="" class="control-label">
                 <label for="AdminPatent_image">Изображение</label>            </label>
                 <div class="controls">
                    {!! Form::file('image', ['id' => 'id-input-filed', 'onchange' => 'previewFile()']) !!}
                      @if ($errors->has('image'))
                        <span class="help-block">
                           <strong>{!! $errors->first('image') !!}</strong>
                        </span>
                     @endif
                 </div>
                 @if (!is_null($item->image) && $item->image !== '')
                    <img id="poster" src="/public/images/{!! $item->image !!}" style="margin-left: 2.5%; height: 80px" alt="Постер...">
                 @else
                    <img id="poster" src="" style="height: 80px" alt="Постер..." style="display: none; margin-left: 2.5%; height: 80px">
                 @endif
              </div>
              <div class="form-actions">
                  {!! Form::submit('Обновить', ['name' => 'yt0', 'class' => 'btn btn-info']) !!}
                  {!! Form::input('reset', null, 'Отмена', ['value' => 'Отмена', 'name' => 'yt1', 'class' => 'btn']) !!}      
              </div>
          {{ Form::close() }}
        </div>
      @endif
   </div>
   <script>      
    function previewFile() {
      var preview = document.querySelector('img#poster');
      var file    = document.querySelector('input[type=file]').files[0];
      var reader  = new FileReader();
      reader.onloadend = function () {
          preview.src = reader.result;
      }

      if (file) {
        reader.readAsDataURL(file);
        if (!$('#poster').is(':visible')) {
            $('#poster').toggle();
        }
      } else {
        review.src = "";
        if ($('#poster').is(':visible')) {
            $('#poster').toggle();
        }
      }
    }          
  </script>