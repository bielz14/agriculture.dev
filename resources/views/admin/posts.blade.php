@extends('layouts.admin')
@section('content')
  <div class="page-content">
    <div class="page-header position-relative" style="border-bottom:none;padding-bottom:0;">
        <ul class="nav nav-tabs" style="border-bottom:none;">
           <li id="tab-list" class="active"><a href="/">Таблица</a></li>
           @if(app('request')->input('id'))
            <li id="tab-update"><a href="/">Обновить</a></li>
            <li id="tab-back-create"><a href="/admin/posts">Вернуться к созданию</a></li>
           @else
            <li id="tab-create"><a href="/">Создать</a></li>
           @endif
        </ul>
    </div>
    @include('admin.list-posts')
    @include('admin.create-update-post')
  </div>
  {{ HTML::script('/public/js/jquery.min.js') }}
  <script src="/public/js/main.js"></script>

  @if (app('request')->input('id'))
    <script type="text/javascript">
      $(document).ready(function(){
        $('#tab-update').addClass('active');
        $('#tab-list').removeClass('active');

        $('.row-fluid.create-update').show();
        $('.row-fluid.list').hide();
      });
     </script>
  @elseif(Session::has('post'))
     <script type="text/javascript">
      $(document).ready(function(){
        $('#tab-create').addClass('active');
        $('#tab-list').removeClass('active');

        $('.row-fluid.create-update').show();
        $('.row-fluid.list').hide();
      });
     </script>
  @endif
@endsection