<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <meta name="description" content="@yield('meta_description')">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>@yield('title')</title>
  <link rel="icon" href="/public/img/core-img/favicon.ico">
  <link rel="stylesheet" href="/public/css/style.css">
  <link rel="stylesheet" href="/public/css/style-root.css">
</head>

<body>
  <!-- Preloader -->
  <div class="preloader d-flex align-items-center justify-content-center">
    <div class="spinner"></div>
  </div>

  <!-- ##### Header Area Start ##### -->
  <header class="header-area">

    <!-- Navbar Area -->
    <div class="famie-main-menu">
      <div class="classy-nav-container breakpoint-off">
        <div class="container">
          <!-- Menu -->
          <nav class="classy-navbar justify-content-between" id="famieNav">
            <!-- Navbar Toggler -->
            <div class="classy-navbar-toggler">
              <span class="navbarToggler"><span></span><span></span><span></span></span>
            </div>
            <!-- Menu -->
            <div class="classy-menu" style="margin:0 auto;">
              <!-- Close Button -->
              <div class="classycloseIcon">
                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
              </div>
              <!-- Navbar Start -->
              <div class="classynav">
                <ul>
                  <li class="active"><a href="/">Home</a></li>
                  <li><a href="/news">News</a></li>
                  <li><a href="/varieties">Varieties</a></li>
                  <li><a href="/patents">Patents</a></li>
                </ul>
              </div>
              <!-- Navbar End -->
            </div>
          </nav>
        </div>
      </div>
    </div>
  </header>
  <!-- ##### Header Area End ##### -->

  @yield('content')

  <!-- ##### Footer Area Start ##### -->
  <footer class="footer-area">

    <!-- Copywrite Area  -->
    <div class="copywrite-area">
      <div class="container">
        <div class="copywrite-text">
          <div class="row align-items-center">
            <div class="col-md-6">
              <p>Copyright &copy;2019 All rights reserved</p>
            </div>
            <div class="col-md-6">
              <div class="footer-nav">
                <nav>
                  <ul>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Produce</a></li>
                    <li><a href="#">Practice</a></li>
                    <li><a href="#">Products</a></li>
                    <li><a href="#">News</a></li>
                    <li><a href="#">Contact</a></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- ##### Footer Area End ##### -->

  <!-- ##### All Javascript Files ##### -->
  <!-- jquery 2.2.4  -->
  <script src="/public/js/jquery.min.js"></script>
  <!-- Popper js -->
  <script src="/public/js/popper.min.js"></script>
  <!-- Bootstrap js -->
  <script src="/public/js/bootstrap.min.js"></script>
  <!-- Owl Carousel js -->
  <script src="/public/js/owl.carousel.min.js"></script>
  <!-- Classynav -->
  <script src="/public/js/classynav.js"></script>
  <!-- Wow js -->
  <script src="/public/js/wow.min.js"></script>
  <!-- Sticky js -->
  <script src="/public/js/jquery.sticky.js"></script>
  <!-- Magnific Popup js -->
  <script src="/public/js/jquery.magnific-popup.min.js"></script>
  <!-- Scrollup js -->
  <script src="/public/js/jquery.scrollup.min.js"></script>
  <!-- Active js -->
  <script src="/public/js/active.js"></script>
</body>

</html>
